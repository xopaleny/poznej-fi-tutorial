# Sculptris
Sculptris je softvér pre tvorbu 3D modelov pomocou simulácie digitálnej hliny. Bol vytvorený tvorcami populárneho profesionálneho softvéru ZBrush, je ale o mnoho jednoduchší. Občas rád padá, tak si nezabudnite ukladať svoju prácu čo najčastejšie.

Každý model v sculptrise sa skladá z malých trojuholníkov, ktorých polohu manipulujete pomocou myši či tabletu a nástrojov. Na množstvo týchto trojuholníkov sa v Sculptrise referuje ako na 'detail'. Ak je týchto trojuholníkov priveľa, model môže byť príliž náročný na to aby bol vhodný do hry.

## Základné ovládanie
Na ľavej strane obrazovky sa nachádzajú základné nástroje. Hore v strede sa nachádzajú nastavenia týchto nástrojov.

- Ctrl + S - uložíte svoju prácu
- W - zobrazíte trojuholníky
- F1 - zobraziť nápovedu (anglicky)

### Ovládanie kamery - myšou
- Ľavý klik na pozadie - otočenie kamery
- Alt + ľavý klik na pozadie; Alt +  - posúvanie kamery
- Koliesko myši - priblíženie/oddialenie kamery

### Ovládanie kamery - tabletom
- Klik na pozadie - otočenie kamery
- Alt + klik na pozadie / Alt + tlačítko na pere + klik  - posúvanie kamery
- Ctrl + tlačítko na pere + klik - priblíženie/oddialenie kamery

### Ovládanie nástrojov
 - Ľavý klik (resp. tablet klik) - použije zvolený nástroj
 - Alt + Ľavý klik (resp. tablet klik) - použije zvolený nástroj 'naopak' (napr. pridanie hmoty odoberá)
 - Medzerník - veľkosť, sila, detail, tvrdosť.



## Modelovanie

Našim cieľom je vymodelovať humanoidnú postavu - chceme teda 2 ruky a 2 nohy. Postava by mala byť vhodná pre hru, takže by nemala byť príliž detailná. V ľavom dolnom rohu sa nachádza počítadlo trojuhoľníkov, snažte sa ho držať pod 250 000.  Ak toto číslo začnete presahovať, skoro určite je oblasť kde sa dá množstvo trojuholníkov zredukovať pomocou Reduce, ako je vysvetlené nižšie.

![](images/triangles.png)

### Základné nástroje
- Grab (chytiť) - posunie hmotu 

![](images/grab.png)

- Draw (kresliť) - pridá hmotu

![](images/draw.png)

- Smooth (vyhladiť) - vyhladí hmotu. Veľmi častý nástroj, takže sa dá tiež aktivovať držaním Shift pri ľubovoľnom inom nástroji.

![](images/smooth.png)

- Crease (rýpať) - vytvorí priehlbinu

![](images/crease.png)

- Reduce (zredukovať) - zjednoduší geometriu modelu podľa nastaveného detailu

![](images/reduce.png)


### Pokročilejšie nástroje

- Flatten (vyrovnať) - vyrovná hmotu do roviny

- Inflate (nafúknuť) - vytvorí 'bublinu' hmoty

- Pinch (uštipnúť) - nahrnie hmotu na miesto, ako keď 'uštipnete' látku

## Maľovanie

Aby ste prešli do módu maľovania, kliknite na 'Paint' v hornom menu. Nechajtre resolution na 512, a kliknite na OK. Pozor, toto je jednosmerná operácia, nedá sa vrátiť naspäť do módu modelovania! 

![](images/paint.png)

### Základné nástroje

- Paint color (malovať farbou) - namaluje hmotu farbou

![](images/paint-color.png)

- Zmena Farby
  
![](images/change_color.png)
### Pokročilejšie nástroje
- Fill (vyplniť) - vyplní povrch hmotu farbou
  
- Paint bump (malovať nerovnosti) - vytvorí falošné nerovnosti
- Flatten bump (vyrovnať nerovnosti) - vyrovná falošné nerovnosti

## Export

Postavu si uložte ako `hero.obj` súbor kliknutím na toto tlačítko.
![](/images/export.png)

Uložte si taktiež textúru ako `hero_albedo.png` súbor, a falošné nerovnosti ako `hero_normal.png`, kliknutím na tieto tlačítka.

![](/images/save-albedo.png)
![](images/save-normals.png)
 