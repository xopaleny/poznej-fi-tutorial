# Mixamo

Mixamo je online služba pre rigovanie - zostavenie digitálnej kostry, podľa ktorej sa náš model bude hýbať. Tento proces sa obvykle robí ručne v nástrojoch ako Blender alebo Maya, ale je možné ho tiež vykonať automaticky online, z dostačujúcim výsledkami. Tento rig (kostra) bude následne využívaný v Unity.

## Postup

- Prihláste sa na stránke https://www.mixamo.com/, ak už nie ste prihlásený.
- Kliknite na tlačítko "Upload character" a vyberte svoj model (ak ste nasledovali predchádzajúci tutoriál, mal by sa volať `hero.obj`).
  
![](./mixamo-images/upload.png)

- Uistite sa že vaša postava je nasmerovaná tvárou do kamery a nohami nadol (použite tlačítka dole v ľavo na otáčanie modelom, ak je to potrebné), a stlačte tlačidlo Next (ďalej).
  
![](./mixamo-images/orient.png)

- Umiestnite značky na svoj model (chin - brada, wrists - zápästia, elbows - lakte, knees - kolená, groin rozkrok). Zvolte Skeleton LOD 'No Fingers' (detail kostry 'žiadne prsty') a stlačte Next.

![](./mixamo-images/markers.png)

- Skontrolujte rig na ukážkovej animácií. Malé nedokonalosti neprekážajú. Pri reálnom vývoji hier by boli opravené vo vyššie spomínaných nástrojoch. Stlačte Next.

![](./mixamo-images/preview.png)

- Môžte si prezrieť iné ukážkové animácie z Mixamo, ale pre Unity potrebujeme iba narigovaný charakter. Uistite sa že krížikom odstránite všetky animácie ktoré ste si prezerali, stlačte tlačídlo 'Download'.

![](./mixamo-images/download.png)

- Zvoľte nastavenia podľa obrázku (FBX for Unity a T-Pose). Stlačte Download.
  
![](./mixamo-images/dl-settings.png)